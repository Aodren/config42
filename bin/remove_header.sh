#!/bin/bash

for file in `find . -name "*.[ch]"`
do
    if [[ `head -1 $file` == "/* ************************************************************************** */" ]]
    then
        nb_line=$(echo "`cat $file|wc -l` - 12" | bc)
        mv $file $file.back
        tail -$nb_line $file.back > $file
    fi
done
