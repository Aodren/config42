#!/bin/bash

for file in `find . -name "*.[ch]"`
do
    vim -u /usr/share/vim/vimrc +Stdheader +wq $file
done
