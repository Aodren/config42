###############################################################################
#                               History                                       #
###############################################################################
#     a voir
bindkey '^[[A' up-line-or-search
bindkey '^[[B' down-line-or-search
up-line-or-search-prefix () {
    local CURSOR_before_search=$CURSOR
    zle up-line-or-search "$LBUFFER"
    CURSOR=$CURSOR_before_search
}
zle -N up-line-or-search-prefix

up-line-or-history-beginning-search () {
    if [[ -n $PREBUFFER ]]; then
        zle up-line-or-history
    else
        zle history-beginning-search-backward
    fi
}
zle -N up-line-or-history-beginning-search
man ()
{
    env \
        LESS_TERMCAP_mb=$(printf "\e[1;31m") \
        LESS_TERMCAP_md=$(printf "\e[0;31m") \
        LESS_TERMCAP_me=$(printf "\e[0;36m") \
        LESS_TERMCAP_se=$(printf "\e[0;33m") \
        LESS_TERMCAP_so=$(printf "\e[1;44;33m") \
        LESS_TERMCAP_ue=$(printf "\e[0;36m") \
        LESS_TERMCAP_us=$(printf "\e[1;36m") \
        PAGER="${commands[less]:-$PAGER}" \
        _NROFF_U=1 \
        PATH="$HOME/bin:$PATH" \
        man "$@"
}
###############################################################################
#                                   COLORS                                    #
###############################################################################
#3 -> yellow
#4 -> blue
#5 -> magenta
#'=(#b) #(--[a-z-]#)=34=36=33'
#
#
#
#zstyle ':completion:*:commands' list-colors "=*=38;36"
#zstyle ':completion:*:commands' list-colors "=*=31;40"
#zstyle ':completion:*:default' list-colors "31,40:"
zstyle ':completion:*:commands' list-colors "di=36;40:ln=1;32;40:so=0;46:pi=33;40:ex=31;40:bd=34;46:cd=34;43:su=0;41:sg=0;46:tw=0;42:ow=0;43:"
#zstyle ':completion:*' list-colors "di=1;32;40:ln=36;40:so=0;46:pi=33;40:ex=31;40:bd=34;46:cd=34;43:su=0;41:sg=0;46:tw=0;42:ow=0;43:"
zstyle ':completion:*' list-colors "di=36;40:ln=1;32;40:so=0;46:pi=33;40:ex=31;40:bd=34;46:cd=34;43:su=0;41:sg=0;46:tw=0;42:ow=0;43:"
zstyle ':completion:*:' list-colors "31,40:"
autoload -U colors && colors
colors
autoload -Uz compinit
compinit
###############################################################################
#                                   PROMPT                                    #
###############################################################################

setopt prompt_subst
autoload colors zsh/terminfo
colors


function __git_prompt {
local DIRTY="%{$fg[yellow]%}"
local CLEAN="%{$fg[green]%}"
local UNMERGED="%{$fg[red]%}"
#local RESET="%{$terminfo[sgr0]%}"
local RESET="%{$fg[red]%}"
git rev-parse --git-dir >& /dev/null

if [[ $? == 0 ]]
then
    echo -n "["
    if [[ `git ls-files -u >& /dev/null` == '' ]]
    then
        git diff --quiet >& /dev/null
        if [[ $? == 1 ]]
        then
            echo -n $DIRTY
        else
            git diff --cached --quiet >& /dev/null
            if [[ $? == 1 ]]
            then
                echo -n $DIRTY
            else
                echo -n $CLEAN
            fi
        fi
    else
        echo -n $UNMERGED
    fi
    echo -n "on branch "
    echo -n `git branch | grep '* ' | sed 's/..//'`
    #echo -n `git status`
    echo -n $RESET
    echo -n "]"
else
    local RESET="%{$terminfo[cyan]%}"
fi
}

#export RPS1='$(__git_prompt) %{$fg[red]%}'
export RPS1='$(__git_prompt) '

PROMPT="%{$fg[cyan]%}%n%{$reset_color%}%{$fg[red]%} at %{$reset_color%}%{$fg[cyan]%}%M%{$reset_color%}%{$fg[red]%} %T %{$reset_color%} %{$fg[cyan]%}%B%~%b %{$reset_color%}%
%{$fg[cyan]%}
-> ";
preexec() { printf "\e[0m"; }

export PROMPT
#export RPROMPT
#LSCOLORS="Cagaagdxbxegedabagacad"
LSCOLORS="gaCaagdxbxegedabagacad"
export LSCOLORS
###############################################################################
#                               AUTO COMPLETION                               #
###############################################################################
autoload -U compinit
compinit
###############################################################################
#                                   ALIASES                                   #
###############################################################################
#Editeurs
alias e='vim'
alias v='vim'
#LS
alias ls='ls -lG'
alias ls='ls -G'
alias ll='ls -lG'
alias lla='ls -laG'
alias la='ls -aG'
