set nocompatible
" init pathogen
call pathogen#infect()
call pathogen#helptags()
set nu
" config colors
syntax enable

"set background=light
let g:solarized_termcolors=256



function Rand()
	return str2nr(matchstr(reltimestr(reltime()), '\v\.@<=\d+')[1:])
endfunction

g:solarized_contrast="normal"
let colorR=Rand()
let height=winheight(".")
let width=winwidth(".")
if colorR > 50000 && height < 40 && width < 100
	set background=light
else
	set background=dark
endif

colorscheme solarized

:command Tree NERDTree

"" config syntastic :help syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

:let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
""includes path
source /nfs/zfs-student-2/users/abary/.vim/path/*.vimrc
"let g:syntastic_c_include_dirs = ['/nfs/zfs-student-2/users/abary/projects/printf/libft/includes']

"Norme motherfuckers!!!

